/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.Entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "remittance", schema="mobex")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Remittance.findAll", query = "SELECT r FROM Remittance r"),
    @NamedQuery(name = "Remittance.findByPkRemittanceId", query = "SELECT r FROM Remittance r WHERE r.pkRemittanceId = :pkRemittanceId"),
    @NamedQuery(name = "Remittance.findByCurrencyFrom", query = "SELECT r FROM Remittance r WHERE r.currencyFrom = :currencyFrom"),
    @NamedQuery(name = "Remittance.findByCurrencyTo", query = "SELECT r FROM Remittance r WHERE r.currencyTo = :currencyTo"),
    @NamedQuery(name = "Remittance.findByForexRate", query = "SELECT r FROM Remittance r WHERE r.forexRate = :forexRate"),
    @NamedQuery(name = "Remittance.findByAmountFrom", query = "SELECT r FROM Remittance r WHERE r.amountFrom = :amountFrom"),
    @NamedQuery(name = "Remittance.findByRemittanceFee", query = "SELECT r FROM Remittance r WHERE r.remittanceFee = :remittanceFee"),
    @NamedQuery(name = "Remittance.findByRemittanceMonth", query = "SELECT r FROM Remittance r WHERE r.remittanceMonth = :remittanceMonth"),
    @NamedQuery(name = "Remittance.findByLatestServerTransactionDate", query = "SELECT r FROM Remittance r WHERE r.latestServerTransactionDate = :latestServerTransactionDate"),
    @NamedQuery(name = "Remittance.findByLatestDeviceTransactionDate", query = "SELECT r FROM Remittance r WHERE r.latestDeviceTransactionDate = :latestDeviceTransactionDate"),
    @NamedQuery(name = "Remittance.findByRemittanceControlNumber", query = "SELECT r FROM Remittance r WHERE r.remittanceControlNumber = :remittanceControlNumber")})
public class Remittance implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pk_remittance_id")
    private Long pkRemittanceId;
    @Size(max = 4)
    @Column(name = "currency_from")
    private String currencyFrom;
    @Size(max = 4)
    @Column(name = "currency_to")
    private String currencyTo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "forex_rate")
    private Double forexRate;
    @Column(name = "amount_from")
    private Double amountFrom;
    @Column(name = "remittance_fee")
    private Double remittanceFee;
    @Size(max = 6)
    @Column(name = "remittance_month")
    private String remittanceMonth;
    @Column(name = "latest_server_transaction_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date latestServerTransactionDate;
    @Column(name = "latest_device_transaction_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date latestDeviceTransactionDate;
    @Size(max = 2147483647)
    @Column(name = "remittance_control_number")
    private String remittanceControlNumber;
    @OneToMany(mappedBy = "fkRemittanceId")
    private Collection<RemittanceTrail> remittanceTrailCollection;
    @JoinColumn(name = "fk_device_id", referencedColumnName = "pk_device_id")
    @ManyToOne
    private Device fkDeviceId;
    @JoinColumn(name = "fk_cfg_remittance_status_id", referencedColumnName = "pk_cfg_remittance_status_id")
    @ManyToOne
    private CfgRemittanceStatus fkCfgRemittanceStatusId;
    @JoinColumn(name = "fk_appuser_id", referencedColumnName = "pk_appuser_id")
    @ManyToOne
    private Appuser fkAppuserId;

    public Remittance() {
    }

    public Remittance(Long pkRemittanceId) {
        this.pkRemittanceId = pkRemittanceId;
    }

    public Long getPkRemittanceId() {
        return pkRemittanceId;
    }

    public void setPkRemittanceId(Long pkRemittanceId) {
        this.pkRemittanceId = pkRemittanceId;
    }

    public String getCurrencyFrom() {
        return currencyFrom;
    }

    public void setCurrencyFrom(String currencyFrom) {
        this.currencyFrom = currencyFrom;
    }

    public String getCurrencyTo() {
        return currencyTo;
    }

    public void setCurrencyTo(String currencyTo) {
        this.currencyTo = currencyTo;
    }

    public Double getForexRate() {
        return forexRate;
    }

    public void setForexRate(Double forexRate) {
        this.forexRate = forexRate;
    }

    public Double getAmountFrom() {
        return amountFrom;
    }

    public void setAmountFrom(Double amountFrom) {
        this.amountFrom = amountFrom;
    }

    public Double getRemittanceFee() {
        return remittanceFee;
    }

    public void setRemittanceFee(Double remittanceFee) {
        this.remittanceFee = remittanceFee;
    }

    public String getRemittanceMonth() {
        return remittanceMonth;
    }

    public void setRemittanceMonth(String remittanceMonth) {
        this.remittanceMonth = remittanceMonth;
    }

    public Date getLatestServerTransactionDate() {
        return latestServerTransactionDate;
    }

    public void setLatestServerTransactionDate(Date latestServerTransactionDate) {
        this.latestServerTransactionDate = latestServerTransactionDate;
    }

    public Date getLatestDeviceTransactionDate() {
        return latestDeviceTransactionDate;
    }

    public void setLatestDeviceTransactionDate(Date latestDeviceTransactionDate) {
        this.latestDeviceTransactionDate = latestDeviceTransactionDate;
    }

    public String getRemittanceControlNumber() {
        return remittanceControlNumber;
    }

    public void setRemittanceControlNumber(String remittanceControlNumber) {
        this.remittanceControlNumber = remittanceControlNumber;
    }

    @XmlTransient
    public Collection<RemittanceTrail> getRemittanceTrailCollection() {
        return remittanceTrailCollection;
    }

    public void setRemittanceTrailCollection(Collection<RemittanceTrail> remittanceTrailCollection) {
        this.remittanceTrailCollection = remittanceTrailCollection;
    }

    public Device getFkDeviceId() {
        return fkDeviceId;
    }

    public void setFkDeviceId(Device fkDeviceId) {
        this.fkDeviceId = fkDeviceId;
    }

    public CfgRemittanceStatus getFkCfgRemittanceStatusId() {
        return fkCfgRemittanceStatusId;
    }

    public void setFkCfgRemittanceStatusId(CfgRemittanceStatus fkCfgRemittanceStatusId) {
        this.fkCfgRemittanceStatusId = fkCfgRemittanceStatusId;
    }

    public Appuser getFkAppuserId() {
        return fkAppuserId;
    }

    public void setFkAppuserId(Appuser fkAppuserId) {
        this.fkAppuserId = fkAppuserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkRemittanceId != null ? pkRemittanceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Remittance)) {
            return false;
        }
        Remittance other = (Remittance) object;
        if ((this.pkRemittanceId == null && other.pkRemittanceId != null) || (this.pkRemittanceId != null && !this.pkRemittanceId.equals(other.pkRemittanceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.Entity.Remittance[ pkRemittanceId=" + pkRemittanceId + " ]";
    }
    
}
