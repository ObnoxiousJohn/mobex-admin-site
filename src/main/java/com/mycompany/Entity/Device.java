/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "device", schema="mobex")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Device.findAll", query = "SELECT d FROM Device d"),
    @NamedQuery(name = "Device.findByPkDeviceId", query = "SELECT d FROM Device d WHERE d.pkDeviceId = :pkDeviceId"),
    @NamedQuery(name = "Device.findByDeviceUuid", query = "SELECT d FROM Device d WHERE d.deviceUuid = :deviceUuid")})
public class Device implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pk_device_id")
    private Long pkDeviceId;
    @Size(max = 2147483647)
    @Column(name = "device_uuid")
    private String deviceUuid;
    @JoinColumn(name = "fk_appuser_id", referencedColumnName = "pk_appuser_id")
    @ManyToOne
    private Appuser fkAppuserId;
    @OneToMany(mappedBy = "fkDeviceId")
    private Collection<Remittance> remittanceCollection;

    public Device() {
    }

    public Device(Long pkDeviceId) {
        this.pkDeviceId = pkDeviceId;
    }

    public Long getPkDeviceId() {
        return pkDeviceId;
    }

    public void setPkDeviceId(Long pkDeviceId) {
        this.pkDeviceId = pkDeviceId;
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }

    public Appuser getFkAppuserId() {
        return fkAppuserId;
    }

    public void setFkAppuserId(Appuser fkAppuserId) {
        this.fkAppuserId = fkAppuserId;
    }

    @XmlTransient
    public Collection<Remittance> getRemittanceCollection() {
        return remittanceCollection;
    }

    public void setRemittanceCollection(Collection<Remittance> remittanceCollection) {
        this.remittanceCollection = remittanceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkDeviceId != null ? pkDeviceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Device)) {
            return false;
        }
        Device other = (Device) object;
        if ((this.pkDeviceId == null && other.pkDeviceId != null) || (this.pkDeviceId != null && !this.pkDeviceId.equals(other.pkDeviceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.Entity.Device[ pkDeviceId=" + pkDeviceId + " ]";
    }
    
}
