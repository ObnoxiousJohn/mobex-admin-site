/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.Entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "appuser", schema = "mobex")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Appuser.findAll", query = "SELECT a FROM Appuser a"),
    @NamedQuery(name = "Appuser.findByPkAppuserId", query = "SELECT a FROM Appuser a WHERE a.pkAppuserId = :pkAppuserId"),
    @NamedQuery(name = "Appuser.findByAuthid", query = "SELECT a FROM Appuser a WHERE a.authid = :authid"),
    @NamedQuery(name = "Appuser.findByServerRegistrationDate", query = "SELECT a FROM Appuser a WHERE a.serverRegistrationDate = :serverRegistrationDate"),
    @NamedQuery(name = "Appuser.findByMpin", query = "SELECT a FROM Appuser a WHERE a.mpin = :mpin"),
    @NamedQuery(name = "Appuser.findByRetries", query = "SELECT a FROM Appuser a WHERE a.retries = :retries"),
    @NamedQuery(name = "Appuser.findByAccountStatus", query = "SELECT a FROM Appuser a WHERE a.accountStatus = :accountStatus")})
public class Appuser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pk_appuser_id")
    private Long pkAppuserId;
    @Size(max = 2147483647)
    @Column(name = "authid")
    private String authid;
    @Column(name = "server_registration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date serverRegistrationDate;
    @Size(max = 2147483647)
    @Column(name = "mpin")
    private String mpin;
    @Column(name = "retries")
    private Integer retries;
    @Column(name = "account_status")
    private Integer accountStatus;
    @OneToMany(mappedBy = "fkAppuserId")
    private Collection<Device> deviceCollection;
    @OneToMany(mappedBy = "fkAppuserId")
    private Collection<Remittance> remittanceCollection;

    public Appuser() {
    }

    public Appuser(Long pkAppuserId) {
        this.pkAppuserId = pkAppuserId;
    }

    public Long getPkAppuserId() {
        return pkAppuserId;
    }

    public void setPkAppuserId(Long pkAppuserId) {
        this.pkAppuserId = pkAppuserId;
    }

    public String getAuthid() {
        return authid;
    }

    public void setAuthid(String authid) {
        this.authid = authid;
    }

    public Date getServerRegistrationDate() {
        return serverRegistrationDate;
    }

    public void setServerRegistrationDate(Date serverRegistrationDate) {
        this.serverRegistrationDate = serverRegistrationDate;
    }

    public String getMpin() {
        return mpin;
    }

    public void setMpin(String mpin) {
        this.mpin = mpin;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public Integer getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
    }

    @XmlTransient
    public Collection<Device> getDeviceCollection() {
        return deviceCollection;
    }

    public void setDeviceCollection(Collection<Device> deviceCollection) {
        this.deviceCollection = deviceCollection;
    }

    @XmlTransient
    public Collection<Remittance> getRemittanceCollection() {
        return remittanceCollection;
    }

    public void setRemittanceCollection(Collection<Remittance> remittanceCollection) {
        this.remittanceCollection = remittanceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkAppuserId != null ? pkAppuserId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Appuser)) {
            return false;
        }
        Appuser other = (Appuser) object;
        if ((this.pkAppuserId == null && other.pkAppuserId != null) || (this.pkAppuserId != null && !this.pkAppuserId.equals(other.pkAppuserId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.Entity.Appuser[ pkAppuserId=" + pkAppuserId + " ]";
    }
    
}
