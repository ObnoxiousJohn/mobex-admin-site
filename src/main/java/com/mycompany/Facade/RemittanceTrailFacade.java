/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.Facade;

import com.mycompany.Entity.RemittanceTrail;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author USER
 */
@Stateless
public class RemittanceTrailFacade extends AbstractFacade<RemittanceTrail> {
    @PersistenceContext(unitName = "com.mycompany_mobexAdmin_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RemittanceTrailFacade() {
        super(RemittanceTrail.class);
    }
    
}
