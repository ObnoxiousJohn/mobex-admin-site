package com.mycompany.Entity;

import com.mycompany.Entity.Appuser;
import com.mycompany.Entity.Remittance;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-03-03T10:14:02")
@StaticMetamodel(Device.class)
public class Device_ { 

    public static volatile SingularAttribute<Device, String> deviceUuid;
    public static volatile CollectionAttribute<Device, Remittance> remittanceCollection;
    public static volatile SingularAttribute<Device, Long> pkDeviceId;
    public static volatile SingularAttribute<Device, Appuser> fkAppuserId;

}