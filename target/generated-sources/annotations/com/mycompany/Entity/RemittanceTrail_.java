package com.mycompany.Entity;

import com.mycompany.Entity.CfgRemittanceStatus;
import com.mycompany.Entity.Remittance;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-03-03T10:14:02")
@StaticMetamodel(RemittanceTrail.class)
public class RemittanceTrail_ { 

    public static volatile SingularAttribute<RemittanceTrail, Long> pkRemittanceTrailId;
    public static volatile SingularAttribute<RemittanceTrail, Date> serverTransactionDate;
    public static volatile SingularAttribute<RemittanceTrail, CfgRemittanceStatus> fkCfgRemittanceStatusId;
    public static volatile SingularAttribute<RemittanceTrail, Date> deviceTransactionDate;
    public static volatile SingularAttribute<RemittanceTrail, Remittance> fkRemittanceId;

}